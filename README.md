# Guter Start/KECK

Die kommunalen Familienportale von [Guter Start NRW](https://www.guterstart.nrw.de/ ) bieten die Möglichkeit Angebote für Zielgruppe bis einschließlich zum Grundschulalter zu sammeln und diese zum Ausgangspunkt von Planungen zu machen. In das Portal können Kennzahlen eingefügt werden. Jedoch nutzen viele Kommunen den [KECK-Altas der Bertelsmann Stiftung](https://www.keck-atlas.de/ ) für die kleinräumige Sozialplanung.

Aus diesem Grund ermöglicht die Software aus dem Angebotsexport-Datensatz aus Guter Start NRW die Inhalte für einen Import in den KECK-Atlas aufzubereiten und somit die Sozialplanung mit aktuellen Angebotsdaten zu unterstützen.

Weitere Informationen kann man der Datei "Guter_Start_KECK.pdf" im Repository entnehmen.
